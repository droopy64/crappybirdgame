package com.it_system.crappybirdgame.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.it_system.crappybirdgame.Util.Constants;

public class MenuState extends State {

    private Texture topBg;
    private Texture bg;

    public MenuState(GameStateManager gameStateManager) {
        super(gameStateManager);
        camera.setToOrtho(false, Constants.WIDTH / 2, Constants.HEIGHT / 2);

        topBg = new Texture("tutorial.png");
        bg = new Texture("bg.png");
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            gameStateManager.set(new PlayState(gameStateManager));
        }
    }

    @Override
    public void update(float dt) {
        handleInput();


    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();
        spriteBatch.draw(bg,0,0);

        //Streach the texture and center it on the screen
        float scalledTopBgWidth = camera.viewportWidth / 2;
        float scalledTopBgHeight = camera.viewportHeight / 2;
        spriteBatch.draw(topBg, camera.position.x - (scalledTopBgWidth / 2),camera.position.y - (scalledTopBgHeight / 2),
                scalledTopBgWidth, scalledTopBgHeight);
        spriteBatch.end();

    }

    @Override
    public void dispose() {
        topBg.dispose();
        bg.dispose();
    }
}
