package com.it_system.crappybirdgame.States;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

public class GameStateManager {
    private Stack<State> states;

    public GameStateManager() {
        states = new Stack<State>();
    }

    //methods to manage stack

    //add
    public void push(State state) {
        states.push(state);
    }

    //remove item
    public void pop() {
        states.pop().dispose();
    }

    public void set(State state) {
        states.pop();           //remove current item
        states.push(state);     //add new item
    }

    public void update(float dt) {
        states.peek().update(dt);
    }

    public void render(SpriteBatch spriteBatch) {
        states.peek().render(spriteBatch);
    }
}
