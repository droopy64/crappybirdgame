package com.it_system.crappybirdgame.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.it_system.crappybirdgame.Util.Constants;

public class GameOverState extends State {
    private Texture bg;
    private Texture gameOver;
    private BitmapFont highestScore;
    private BitmapFont yourScore;
    private Texture playAgainBtn;

    public GameOverState(GameStateManager gameStateManager) {
        super(gameStateManager);

        camera.setToOrtho(false, Constants.WIDTH / 2, Constants.HEIGHT / 2);
        bg = new Texture("bg.png");
        gameOver = new Texture("gameover.png");
        playAgainBtn = new Texture("start.png");
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            //start a new game
            gameStateManager.set(new PlayState(gameStateManager));
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();

        spriteBatch.draw(bg, 0,0);

        spriteBatch.draw(gameOver,
                camera.position.x - (camera.viewportWidth / 2) + 25,
                camera.position.y + 120);

        //highest score
        highestScore = new BitmapFont(Gdx.files.internal("kenpixel.fnt"));
        highestScore.setColor(Color.WHITE);
        highestScore.getData().setScale(0.5f);
        highestScore.draw(spriteBatch, "Highest Score: ",
                camera.position.x - (camera.viewportWidth / 2) + 50,
                camera.position.y + (camera.viewportHeight / 2) - 110);

        yourScore = new BitmapFont(Gdx.files.internal("kenpixel.fnt"));
        yourScore.setColor(Color.WHITE);
        yourScore.getData().setScale(0.5f);
        yourScore.draw(spriteBatch, "Score: ",
                camera.position.x - (camera.viewportWidth / 2) + 50,
                camera.position.y + (camera.viewportHeight / 2) - 90);

        spriteBatch.draw(playAgainBtn, camera.position.x - (playAgainBtn.getWidth() / 2),
                gameOver.getHeight() + 70);


        spriteBatch.end();
    }

    @Override
    public void dispose() {
        bg.dispose();
        gameOver.dispose();
        playAgainBtn.dispose();
    }
}
