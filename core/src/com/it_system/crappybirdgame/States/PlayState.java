package com.it_system.crappybirdgame.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.it_system.crappybirdgame.Sprites.Bird;
import com.it_system.crappybirdgame.Sprites.Tube;
import com.it_system.crappybirdgame.Util.Constants;

import static com.it_system.crappybirdgame.Util.Constants.GROUND_Y_OFFSET;
import static com.it_system.crappybirdgame.Util.Constants.TUBE_COUNT;
import static com.it_system.crappybirdgame.Util.Constants.TUBE_SPACING;
import static com.it_system.crappybirdgame.Util.Constants.TUBE_WIDTH;

public class PlayState extends State {

    private BitmapFont font;

    private Texture bg;
    private Bird bird;
    private Array<Tube> tubes;

    private Texture ground;
    private Vector2 groundPos1, groundPos2;

    private int pointsCounter = 0;

    public PlayState(GameStateManager gameStateManager) {
        super(gameStateManager);
        bird = new Bird(50, 300);

        camera.setToOrtho(false, Constants.WIDTH / 2, Constants.HEIGHT / 2);

        bg = new Texture("bg.png");

        ground = new Texture("ground.png");
        groundPos1 = new Vector2(camera.position.x - camera.viewportWidth / 2, GROUND_Y_OFFSET);
        groundPos2 = new Vector2(camera.position.x - camera.viewportWidth / 2 + ground.getWidth(), GROUND_Y_OFFSET);

        tubes = new Array<Tube>();

        for (int i = 1; i <= TUBE_COUNT; i++) {
            tubes.add(new Tube(i * (TUBE_SPACING + TUBE_WIDTH)));
        }

    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            bird.jump();
        }

    }

    @Override
    public void update(float dt) {
        handleInput();
        updateGround();
        bird.update(dt);

        hitGround();
        // Gdx.app.log("Position", String.valueOf(bird.getPosition().y));

        //Follow the bird with the camera

        camera.position.x = bird.getPosition().x + 50;

        for (Tube tube : tubes) {
            //check if tube if off the camera
            if (camera.position.x - (camera.viewportHeight / 2 ) > tube.getPosTopTube().x + tube.getTopTube().getWidth()) {
                tube.reposition(tube.getPosTopTube().x + (TUBE_WIDTH + TUBE_SPACING) * TUBE_COUNT);
            }

            //collision check
            if (tube.collides(bird.getBounds())) {
                //Bird died
                gameStateManager.set(new GameOverState(gameStateManager));
            }

            //Check for score collision
            if (tube.hasScored(bird.getBounds()) && tube.getCounter() == 0) {
                pointsCounter += 2;
                tube.setCounter(pointsCounter);
            }
        }

        hitGround();

        camera.update();

    }

    public void hitGround() {
        if (bird.getPosition().y <= ground.getHeight() + GROUND_Y_OFFSET) {
            // Add pause before tutorial is shown
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    gameStateManager.set(new MenuState(gameStateManager));
                }
            }, .3f);
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();

        spriteBatch.draw(bg, camera.position.x -  (camera.viewportWidth / 2), 0);

        //draw the bird to the screen
        spriteBatch.draw(bird.getBird(), bird.getPosition().x, bird.getPosition().y);

        //draw tubes to the screen
        for (Tube tube : tubes) {
            spriteBatch.draw(tube.getTopTube(), tube.getPosTopTube().x, tube.getPosTopTube().y);
            spriteBatch.draw(tube.getBottomTube(), tube.getPosBotTube().x, tube.getPosBotTube().y);
        }

        spriteBatch.draw(ground, groundPos1.x, groundPos1.y);
        spriteBatch.draw(ground, groundPos2.x, groundPos2.y);

        //Draw score on the screen
        font = new BitmapFont(Gdx.files.internal("kenpixel.fnt"));
        font.setColor(Color.BROWN);
        font.getData().setScale(0.5f);
        font.draw(spriteBatch, "Score: " + pointsCounter,
                camera.position.x - (camera.viewportWidth / 2) + 100,
                camera.position.y + (camera.viewportWidth / 2) + 70);

        spriteBatch.end();
    }

    public void updateGround() {
        //check if the last ground is invisible
        if (camera.position.x - (camera.viewportWidth / 2) > groundPos1.x + ground.getWidth()) {
            groundPos1.add(ground.getWidth() * 2, 0);
        }
        if (camera.position.x - (camera.viewportWidth / 2) > groundPos2.x + ground.getWidth()) {
            groundPos2.add(ground.getWidth() * 2, 0);
        }
    }

    @Override
    public void dispose() {
        bg.dispose();
        ground.dispose();
        bird.dispose();

        for (Tube t : tubes) {
            t.dispose();
        }

    }
}
