package com.it_system.crappybirdgame.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

import static com.it_system.crappybirdgame.Util.Constants.FLUCTUATION;
import static com.it_system.crappybirdgame.Util.Constants.LOWEST_OPENING;
import static com.it_system.crappybirdgame.Util.Constants.TUBE_GAP;

public class Tube {
    private Texture topTube;
    private Texture bottomTube;
    private Vector2 posTopTube;
    private Vector2 posBotTube;
    private Random random;
    private Rectangle boundsTop, boundsBot;
    private Rectangle scorebound;
    private int counter;

    public Tube(float x){
        topTube = new Texture("toptube.png");
        bottomTube = new Texture("bottomtube.png");
        random = new Random();

        posTopTube = new Vector2(x + 180, random.nextInt(FLUCTUATION)
                + TUBE_GAP + LOWEST_OPENING);

        posBotTube = new Vector2(x + 180, posTopTube.y - TUBE_GAP - bottomTube.getHeight());

        counter = 0;
        scorebound = new Rectangle(posBotTube.x + bottomTube.getWidth(), posBotTube.y,
                bottomTube.getWidth(), bottomTube.getHeight() + topTube.getHeight() + TUBE_GAP);

        //Set invisible rectangles on top of tubes to make them collision ready
        boundsTop = new Rectangle(posTopTube.x, posTopTube.y, topTube.getWidth(), topTube.getHeight());
        boundsBot = new Rectangle(posBotTube.x, posBotTube.y, bottomTube.getWidth(), bottomTube.getHeight());

    }

    public int getCounter() {
        return counter;
    }

    public Texture getTopTube() {
        return topTube;
    }

    public Texture getBottomTube() {
        return bottomTube;
    }

    public Vector2 getPosTopTube() {
        return posTopTube;
    }

    public Vector2 getPosBotTube() {
        return posBotTube;
    }

    public Random getRandom() {
        return random;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean collides(Rectangle player) {
        return player.overlaps(boundsTop) || player.overlaps(boundsBot);

    }

    public boolean hasScored(Rectangle player) {
        return player.overlaps(scorebound);
    }

    public void reposition(float x) {
        posTopTube.set(x, random.nextInt(FLUCTUATION) + TUBE_GAP + LOWEST_OPENING);
        posBotTube.set(x, posTopTube.y - TUBE_GAP - bottomTube.getHeight());

        //reposition score bound
        scorebound.setPosition(posBotTube.x, posBotTube.y);
    }

    public void dispose() {
        topTube.dispose();
        bottomTube.dispose();
    }
}
