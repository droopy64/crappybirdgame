package com.it_system.crappybirdgame.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import static com.it_system.crappybirdgame.Util.Constants.GRAVITY;
import static com.it_system.crappybirdgame.Util.Constants.MOVEMENT;

public class Bird {
    private Vector2 position;
    private Vector2 velocity;
    private Texture bird;
    private Rectangle bounds;   //for bird collision detection

    public Bird(int x, int y){
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        bird = new Texture("bird.png");

        bounds = new Rectangle(x, y, bird.getWidth(), bird.getHeight());
    }

    public Vector2 getPosition() {
        return position;
    }

    public Texture getBird() {
        return bird;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void update(float dt) {

        if (position.y > 0 ) {
            velocity.add(0, GRAVITY);
        }

        velocity.scl(dt);
        position.add(MOVEMENT * dt, velocity.y);

        // do not allow to drop below the ground level
        if (position.y < 0) {
            position.y = 0;

        }

        velocity.scl(1/dt);

        bounds.setPosition(position.x, position.y);

    }

    public void jump(){
        velocity.y = 200;
    }

    public void dispose() {

    }

}
