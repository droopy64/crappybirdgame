package com.it_system.crappybirdgame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.it_system.crappybirdgame.States.GameStateManager;
import com.it_system.crappybirdgame.States.MenuState;
import com.it_system.crappybirdgame.States.PlayState;

public class CrappyBirdGameMain extends ApplicationAdapter {
	SpriteBatch batch;
	GameStateManager gameStateManager;

	@Override
	public void create () {

		batch = new SpriteBatch();
		gameStateManager = new GameStateManager();

		//push new state to the main screen
		gameStateManager.push(new MenuState(gameStateManager));
	}

	@Override
	public void render () {

		//Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		gameStateManager.update(Gdx.graphics.getDeltaTime());
		gameStateManager.render(batch);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
