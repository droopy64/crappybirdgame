package com.it_system.crappybirdgame.Util;

public class Constants {
    public static final int WIDTH = 480;
    public static final int HEIGHT = 800;
    public static final int GRAVITY = -15;
    public static final int FLUCTUATION = 120;
    public static final int TUBE_GAP = 100;
    public static final int LOWEST_OPENING = 145;
    public static final int TUBE_COUNT = 100; //temporary solution TODO: No collision is detected when tubes are repositioned (bird passed > TUBE_COUNT
    public static final int TUBE_SPACING = 145;
    public static final int TUBE_WIDTH = 52;
    public static final int MOVEMENT = 100;
    public static final int GROUND_Y_OFFSET = -30;
}
